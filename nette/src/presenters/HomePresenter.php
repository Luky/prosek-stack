<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Components\ParserControl\ParserControl;
use App\Model\Service\ParserService;
use Nette\Application\UI\Form;

/**
 * @package App\Presenters
 */
class HomePresenter extends BasePresenter
{
	/* === ATTRIBUTES ========================================================================================= */
	
	/* === STARTUP ============================================================================================ */
	
	public function startup(): void
	{
		parent::startup();
		
	}
	
	/* === ACTIONS ============================================================================================ */
	
	public function actionDefault(): void
	{
	
	}
	
	/* === HANDLES ============================================================================================ */
	
	/* === RENDERS ============================================================================================ */
	
	public function renderDefault(): void
	{
	
	}
	
	/** @var \App\Components\ParserControl\IParserFactory @inject */
	public $factory;
	
	public function createComponentParseControl(): ParserControl
	{
		return $this->factory->create();
	}
	
	public function createComponentDummyForm(): Form
	{
		$form = new Form();
		
		$form->addText('name', 'Name')
			->setAttribute('placeholder', 'Placeholder');
		
		$form->addTextArea('text', 'Text')
			->setAttribute('placeholder', 'Placeholder');
		
		$form->addCheckbox('check', 'Some check');
		
		$form->addRadioList(
			'radio',
			'Radio list',
			[
				'Red',
				'Blue',
				'Green',
			]
		);
		
		$form->addSubmit('submit', 'Submit');
		
		return $form;
	}
}

