<?php declare(strict_types = 1);

namespace App\Model\Service;

use GuzzleHttp\Exception\ClientException;

class ParserService {
	
	private const URL = 'https://is.sps-prosek.cz/suplovani/prosek/';
	
	/** @var \GuzzleHttp\Client */
	private $client;
	
	public function __construct(\GuzzleHttp\Client $client)
	{
		$this->client = $client;
	}
	
	public function parse(int $days = 7): array
	{
		
		$today = new \DateTime('now');
		
		$arr = [];
		
		for ($i = 0; $i < 6; $i++) {
			$mod = clone $today;
			
			$date = $mod->modify("+{$i} days")->format('dmY');
			
			try {
				$url = self::URL . $date . '.pdf';
				$response = $this->client->get($url);
				
				$arr[] = $url;
			} catch (ClientException $e) {
				// skip
			}
			
			
			
		}
		
		return $arr;
	}
	
}
