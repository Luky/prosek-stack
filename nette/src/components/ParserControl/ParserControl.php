<?php declare(strict_types = 1);

namespace App\Components\ParserControl;

use Nette\Application\UI\Control;

class ParserControl extends Control
{
	
	/** @var \App\Model\Service\ParserService */
	private $service;
	
	public function __construct(\App\Model\Service\ParserService $service)
	{
		$this->service = $service;
	}
	
	public function render(): void
	{
		$this->template->urls = $this->service->parse();
		
		$this->template->setFile(__DIR__ . '/parser.latte');
		$this->template->render();
	}
	
}
