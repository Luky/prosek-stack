<?php declare(strict_types = 1);

namespace App\Components\ParserControl;

interface IParserFactory{
	public function create(): ParserControl;
}
