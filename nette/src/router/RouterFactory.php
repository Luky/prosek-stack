<?php declare(strict_types = 1);

namespace App\Lib\Router;

use Nette;
use Nextras;

class RouterFactory
{
	private const SCHEMA = 'https://%host%/';
	
	public function create(): Nette\Application\IRouter
	{
		$router = new Nette\Application\Routers\RouteList();
		
		$router[] = new Nextras\Routing\StaticRouter(
			[
				'Home:default' => '',
				
				// Static Files
				'System:sitemap' => 'sitemap.xml',
				'System:manifest' => 'manifest.json',
			],
			Nette\Application\IRouter::SECURED
		);
		
		$router[] = new Nette\Application\Routers\Route(
			self::SCHEMA . '[<presenter>][/<action>][/<id>]', 'Home:default'
		);
		
		return $router;
	}
	
}
