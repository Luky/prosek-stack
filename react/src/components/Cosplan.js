import React, {Component} from 'react';

var ReactCanvas = require('react-canvas');

const Surface = ReactCanvas.Surface;
const Image = ReactCanvas.Image;
const Text = ReactCanvas.Text;
const Group = ReactCanvas.Group;
const Layer = ReactCanvas.Layer;

const FontFace = ReactCanvas.FontFace;

const backgrounds = {
	1: require('./../../images/layout/1.png'),
	2: require('./../../images/layout/2.png'),
	3: require('./../../images/layout/3.png'),
	4: require('./../../images/layout/4.png'),
	6: require('./../../images/layout/6.png'),
	7: require('./../../images/layout/7.png'),
};


const dragons = [
	{
		image: require('./../../images/dragonsSmall/1.png'),
		w: 279,
		h: 416,
	},
	{
		image: require('./../../images/dragonsSmall/2.png'),
		w: 369,
		h: 420,
	},
	{
		image: require('./../../images/dragonsSmall/3.png'),
		w: 346,
		h: 437,
	},
	{
		image: require('./../../images/dragonsSmall/4.png'),
		w: 408,
		h: 427,
	},
	{
		image: require('./../../images/dragonsSmall/5.png'),
		w: 372,
		h: 407,
	},
	{
		image: require('./../../images/dragonsSmall/6.png'),
		w: 313,
		h: 435,
	},
	{
		image: require('./../../images/dragonsSmall/7.png'),
		w: 323,
		h: 431,
	},
	{
		image: require('./../../images/dragonsSmall/8.png'),
		w: 320,
		h: 414,
	},

	{
		image: require('./../../images/dragonsSmall/9.png'),
		w: 302,
		h: 429,
	},
	{
		image: require('./../../images/dragonsSmall/10.png'),
		w: 278,
		h: 383,
	},

];


export default class Cosplan extends Component {

	constructor(state) {
		super(state);

		this.dragonsSelected = (() = > {
				let array = [];

		let x = 0;
		while (x < 20 && array.length < 3) {
			const num = Math.floor(Math.random() * 10);

			if (array.indexOf(num) === -1) {
				array.push(num);
			}
			x++;
		}
		return array;
	})
		();
	}


	convertWidth(percentage) {
		return percentage * this.props.width;
	}

	converthHeight(percentage) {
		return percentage * this.props.height;
	}


	render() {
		const {
			width,
			height,
			background,
			ratio,
			days,
			name,
			style,
		} = this.props;

		const bit = (() = > {
				let x = 0;

		Object.keys(days).map((key, index) = > {
			const day = days[key];

		if (day.value === true) {
			x += day.bit;
		}
	})
		;

		return x;
	})
		();


		const textStyle = {
			top: 10,
			left: 0,
			width: window.innerWidth,
			height: 20,
			lineHeight: 20,
			fontSize: 12,
		};


		const backgroundStyle = {
			top: 0, left: 0,
			width: width,
			height: height,
		};

		const surfaceStyle = {
			width: '100%',
			height: '100%',
		};

		const IMAGE_LEFT = 0;
		const IMAGE_CENTER = 1;
		const IMAGE_RIGHT = 2;

		const dayImageStyle = [
			{
				top: 164,
				left: 82,
				height: 380,
				width: 298,
			},
			{
				top: 107,
				left: 428,
				height: 380 + 57,
				width: 298 + 46,
			},
			{
				top: 164,
				left: 821,
				height: 380,
				width: 298,
			},
		];


		const smallDragonStyle = {top: 440};

		const dragonSmallRatio = 0.33;

		const dragonDayStyle = [
			{
				...smallDragonStyle,
			left
	:
		310,
	},
		{
		...
			smallDragonStyle,
				left;
		:
			690,
		}
	,
		{
		...
			smallDragonStyle,
				left;
		:
			1060,
		}
	,
	]
		;


		const dragonBigStyle = {top: 180};

		const dragonBigRatio = 0.50;

		const dragonBigDayStyle = [
			{
				...dragonBigStyle,
			left
	:
		148,
	},
		{
		...
			dragonBigStyle,
				left;
		:
			690,
		}
	,
		{
		...
			dragonBigStyle,
				left;
		:
			885,
		}
	,
	]
		;


		const nameFontSize = (l) =
	>
		{
			if (l >= 26) return 16;
			if (l >= 25) return 17;
			if (l >= 23) return 19;
			if (l >= 20) return 22;
			if (l >= 16) return 24;
			if (l >= 14) return 26;
			if (l >= 12) return 30;
			return 32;
		}
		;


		const styleTextBasic = {
			fontSize: nameFontSize(name.length),
			color: 'white',
			width: 300,
			height: 400,
			fontFace: FontFace('Raleway', null, {weight: 100}),

			textAlign: 'center',
		};

		const nameBottom = {...styleTextBasic, top;
	:
		440;
	}
		;
		const nameTop = {...styleTextBasic, top;
	:
		80;
	}
		;

		const nameLeft = {left: 80};
		const nameRight = {left: 820};

		const nameLeftTop = {...nameTop, .;
	..
		nameLeft;
	}
		;
		const nameRightTop = {...nameTop, .;
	..
		nameRight;
	}
		;
		const nameRightBottom = {...nameBottom, .;
	..
		nameRight;
	}
		;


		const dayNameBasic = {
			fontSize: 30,
			top: 20,
			color: '#353535',
			height: 380,
		};

		const dayNameStyle = [
			{
				...dayNameBasic,
			top
	:
		164,
			left;
	:
		82,
			width;
	:
		298,
	},
		{
		...
			dayNameBasic,
				top;
		:
			107,
				left;
		:
			428,
				height;
		:
			380 + 57,
				width;
		:
			298 + 46,
		}
	,
		{
		...
			dayNameBasic,
				top;
		:
			164,
				left;
		:
			821,
				height;
		:
			380,
				width;
		:
			298,
		}
	,
	]
		;


		const layerStyle = {
			top: 30,
			left: 50,
			width: 200,
			heght: 200,
			backgroundColor: 'red',
		};


		const nameStyle = (() = > {
				if (bit === 1;
	)
		{
			return nameRightBottom;
		}
		if (bit === 2) {
			return nameRightBottom;
		}
		if (bit === 4) {
			return nameRightBottom;
		}
		if (bit === 3) {
			return nameLeftTop;
		}
		if (bit === 6) {
			return nameRightTop;
		}
		if (bit === 7) {
			return nameLeftTop;
		}
	})
		();
		const isDragonBig = (() = > {
				// Only middle big
				if (bit === 1;
	)
		{
			return [true, false, true];
		}
		if (bit === 2) {
			return [true, false, true];
		}
		if (bit === 4) {
			return [true, false, true];
		}

		// Pa + So + -
		if (bit === 3) {
			return [false, false, true];
		}

		// -  + So + Ne
		if (bit === 6) {
			return [true, false, false];
		}

		// Pa + So + Ne
		if (bit === 7) {
			return [false, false, false];
		}
	})
		();
		const charactersStyle = (() = > {
				// Only middle big
				if (bit === 1;
	)
		{
			return [dayImageStyle[IMAGE_CENTER], null, null];
		}
		if (bit === 2) {
			return [null, dayImageStyle[IMAGE_CENTER], null];
		}
		if (bit === 4) {
			return [null, null, dayImageStyle[IMAGE_CENTER]];
		}

		// Pa + So + -
		if (bit === 3) {
			return [dayImageStyle[IMAGE_LEFT], dayImageStyle[IMAGE_CENTER], null];
		}

		// -  + So + Ne
		if (bit === 6) {
			return [null, dayImageStyle[IMAGE_CENTER], dayImageStyle[IMAGE_RIGHT]];
		}

		// Pa + So + Ne
		if (bit === 7) {
			return [dayImageStyle[IMAGE_LEFT], dayImageStyle[IMAGE_CENTER], dayImageStyle[IMAGE_RIGHT]];
		}
	})
		();
		const charactersNameStyle = (() = > {
				// Only middle big
				if (bit === 1;
	)
		{
			return [dayNameStyle[IMAGE_CENTER], null, null];
		}
		if (bit === 2) {
			return [null, dayNameStyle[IMAGE_CENTER], null];
			dmpm;
		}
		if (bit === 4) {
			return [null, null, dayNameStyle[IMAGE_CENTER]];
		}

		// Pa + So + -
		if (bit === 3) {
			return [dayNameStyle[IMAGE_LEFT], dayNameStyle[IMAGE_CENTER], null];
		}

		// -  + So + Ne
		if (bit === 6) {
			return [null, dayNameStyle[IMAGE_CENTER], dayNameStyle[IMAGE_RIGHT]];
		}

		// Pa + So + Ne
		if (bit === 7) {
			return [dayNameStyle[IMAGE_LEFT], dayNameStyle[IMAGE_CENTER], dayNameStyle[IMAGE_RIGHT]];
		}
	})
		();

		return (
			< div;
		style = {
		{...
			style, width;
		:
			'100%';
		}
	}>
	<
		div;
		style = {
		{
			fontFamily: '\"Raleway\"';
		}
	}></
		div >
		< Surface;
		id = {this.props.id;
	}
		width = {width};
		height = {height};
		left = {0};
		top = {0};
		style = {surfaceStyle} >

			{/* background */}
			< Image;
		src = {backgrounds[bit]};
		style = {backgroundStyle} / >

			< Text;
		style = {nameStyle} > {name} < / Text >

			{Object.keys(days).map((key, index) = > {
				const day = days[key];

		if (day.value) {

			return (
				< Group;
			key = {index} >
				< Image;
			src = {day.photo;
		}
			style = {charactersStyle[index]} / >
				< Layer;
			style = {layerStyle} >
				< Text;
			style = {charactersNameStyle[index]} > {day.name.toUpperCase();
		}</
			Text >
			< / Layer >

			< / Group >
		)
			;
		}
	})
	}

	<
		Group >
		{isDragonBig.map((isBig, index) = > {
			const dragon = dragons[this.dragonsSelected[index]];
		const mergedStyle =
				isBig === true ? {
					...dragonBigDayStyle[index],
			width;
	:
		dragon.w * dragonBigRatio,
			height;
	:
		dragon.h * dragonBigRatio,
	} :
		{
		...
			dragonDayStyle[index],
				width;
		:
			dragon.w * dragonSmallRatio,
				height;
		:
			dragon.h * dragonSmallRatio,
		}
		;
		return (
			< Image;
		key = {index};
		src = {dragon.image;
	}
		style = {mergedStyle} / >
	)
		;
	})
	}
	</
		Group >

		< / Surface >
		< / div >
	)
		;
	}


}
