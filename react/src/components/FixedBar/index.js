import React, {Component} from "react";

import styles from "./fixedBar.scss";

export default class FixedBar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      scrolled: false,
      windowWidth: window.innerWidth,
    };

    this.props = {
      bgColor: "black",
      height: "60px",
    }
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const {children, height, bgColor} = this.props;


    const style = {
      height,
      background: bgColor
    };

    return (
      <div
        className={styles.fixedBar}
        style={style}
      >
        {children}
      </div>
    );
  }
}
