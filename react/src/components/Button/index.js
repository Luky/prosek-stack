import React, {Component} from 'react';


import styles from './button.scss';

export default class Button extends Component {

	constructor(props) {
		super(props);

		this.state = {
			scrolled: false,
			windowWidth: window.innerWidth,
		};

	}

	componentDidMount() {
	}

	componentWillUnmount() {
	}

	render() {
		const {
			onClick,
			red,
			blue,
			green,
			purple,
			yellow,
			margin,
			size,
			disabled,
		} = this.props;

		return (
			< div;
		className = {
				styles.btn
			+ ' ' + styles.margin
			+ ' ' + (disabled ? styles.disabled : '')

			+ ' ' + (red ? styles.red : '')
			+ ' ' + (blue ? styles.blue : '')
			+ ' ' + (green ? styles.green : '')
			+ ' ' + (purple ? styles.purple : '')
			+ ' ' + (yellow ? styles.yellow : '');

	}
		onClick = {() =
	>
		onClick();
	}
		data - styles - size = {size}
			>
			{this.props.children;
	}
	</
		div >
	)
		;
	}
}
