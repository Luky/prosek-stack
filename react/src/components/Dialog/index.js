import React, {Component} from 'react';

import Step from './../Step';

import styles from './dialog.scss';

export default class Dialog extends Component {

	constructor(props) {
		super(props);

		this.state = {
			scrolled: false,
			windowWidth: window.innerWidth,
		};

	}

	componentDidMount() {
	}

	componentWillUnmount() {
	}

	render() {
		const {
			show,
			padding,
			step,
		} = this.props;

		const style = {
			paddingTop: (padding + padding / 11 * 3) + 'px',
			paddingRight: padding + 'px',
			paddingLeft: padding + 'px',
			paddingBottom: (padding + padding / 3) + 'px',

		};


		return (
			< div;
		className = {styles.wrap + ' ' + (show === true && styles.show);
	}>
	<
		div;
		className = {styles.shadow;
	}/>
	<
		div;
		className = {styles.dialog;
	}
		style = {style} >
			< div;
		className = {styles.content;
	}>
		{
			this.props.children.map((s) = > {
				if (s.type !== Step;
		)
			{
				throw new Error('`Dialog` children should be of type `Step`.');
			}

			if (s.props.step === step) {
				return s;
			}
		})
		}
	</
		div >
		< / div >
		< / div >
	)
		;
	}
}
