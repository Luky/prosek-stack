import React, {Component} from "react";
import {connect} from "react-redux";
import TextareaAutosize from "react-textarea-autosize";
import styles from "./textAreaFacebook.sass";

class TextAreaFacebook extends Component {


  optimizeString(string) {
    const convFromL = String.fromCharCode(283, 353, 269, 345, 382, 253, 225, 237, 233, 357, 367, 250, 243, 271, 328, 318, 314);
    const convToL = String.fromCharCode(101, 115, 99, 114, 122, 121, 97, 105, 101, 116, 117, 117, 111, 100, 110, 108, 108);


    let e = string.toLowerCase();
    let out = new String;
    edg: for (let i = 0; i < e.length; i++) {
      for (let j = 0; j < convFromL.length; j++) {
        if (e.charAt(i) == convFromL.charAt(j)) {
          out += convToL.charAt(j);
          continue edg;
        }
      }
      out += e.charAt(i)
    }
    return out.replace(/[^0-9A-Za-z]{1,}?/g, " ")
      .replace(/^\s*|\s*$/g, "")
      .replace(/[\s]+/g, "-")
      ;
  }


  constructor(props) {
    super(props);
    this.state = {
      fetched: false,
      data: null,
      cachedFriends: [],
      cachedFriendsFetched: false
    };
  }

  searchOnFb(realValue) {
    const {fetched, data, cachedFriends, cachedFriendsFetched} = this.state;

    let value = this.optimizeString(realValue)

    let localData = {
      page: [],
      event: [],
      friend: [],
    };

    if (window.me.accessToken) {
      const types = ["page", "event"];

      types.map((type) => {
        console.log("Serach for " + type);
        FB.api("/search?limit=3", {"access_token": window.me.accessToken, "q": value, "type": type}, (response) => {
          if (!response || response.error) {
            console.error(response);
          } else {
            console.log(type, response.data);
            localData[type].push(...response.data);
            this.setState({fetched: true, data: localData});
          }
        });
      })

      if (cachedFriendsFetched === true) {
        console.log("from cache");
        console.log(cachedFriends);

        cachedFriends.map((f) => {
          console.log(f, f.lower, value);
          if (f.lower.indexOf(value) !== -1) {
            localData.friend.push(f);
          }
        });

        this.setState({fetched: true, data: localData});

      } else {
        console.log("request");
        let friends = [];

        FB.api("me/taggable_friends?limit=5000", {"access_token": window.me.accessToken}, (response) => {
          if (!response || response.error) {
            console.error(response);
          } else {


            response.data.map((f) => {

              f.lower = this.optimizeString(f.name);

              friends.push(f);

            });

            console.log("push this friends ", friends);

            this.setState({cachedFriends: friends, cachedFriendsFetched: true});


            friends.map((f) => {
              console.log(f, f.lower, value);
              if (f.lower.indexOf(value) !== -1) {
                localData.friend.push(f);
              }
            });


            this.setState({fetched: true, data: localData});
          }
        });

      }

    }
  }

  onClick(e) {
    console.log(e);
    console.log(e.target);
    console.log(e.handler);
  }


  render() {
    const {fetched, data, friends} = this.state;

    const {theme, onChange} = this.props;

    let lastWords = [];

    let lw = null;

    //console.log("fetched=>",fetched);
    //console.log("state=>",data);

    return (
      <div>
        <TextareaAutosize
          style={{
						"transition": "all .3s",
						"resize": "none",

						"letterSpacing": "normal",
						"wordSpacing": "normal",
						"textTransform": "none",
						"textIndent": "0px",
						"outline": "0",
						"textShadow": "none",
						"display": "inline-block",
						"textAlign": "start",
						"margin": "0em 0em 0em 0em",
						"font": "13.3333px Arial",

						"MozBackgroundClip": "padding",
						"MozBorderRadius": "0",
						"WebkitBackgroundClip": "padding-box",
						"WebkitBorderRadius": "0",
						"backgroundClip": "padding-box",
						"borderRadius": "0",
						"fontSize": "15px",
						"color": "#53585d",
						"padding": "10px",
						"paddingBottom": "15px",
						"width": "100%",
						"position": "relative",
						"textRendering": "auto",
					}}
          minRows={3}
          maxRows={20}
          onKeyUp={(e) => {

						const key = e.keyCode;

						if (key != 13 || key != 32) {

							let words = e.target.value.replace(/\n/g, " ").split(" ");
							{/*console.log("words", words);*/
							}

							for (let i = 0; i < words.length; i++) {

								if (typeof lastWords[i] === 'undefined' || words[i] != lastWords[i]) {
									lw = words[i];

									if (lw.charAt(0) == "@" && lw.length > 1) {
										this.searchOnFb(lw.substring(1));
									}

									break;
								}
							}

							lastWords = words;
						}

					}}
        />
        {fetched && data &&
        <div className={styles.whisp}>
          {data.friend.map((row) => {
            return (
              <a className={styles.whispItem} onClick={(e) => {
								console.log(e.target);
							}}>
                <img src={row.picture.data.url}/>
                <div>{row.name}</div>
              </a>
            )
          })}
          {data.page.map((row) => {
            return (
              <a className={styles.whispItem} onClick={(e) => this.onClick(e)}>
                <img src={"http://graph.facebook.com/" + row.id + "/picture?type=square"}/>
                <div>{row.name}</div>
              </a>
            )
          })}

          {data.event.map((row) => {
            return (
              <a className={styles.whispItem} onClick={(e) => this.onClick(e)}>
                <img src={"http://graph.facebook.com/" + row.id + "/picture?type=square"}/>
                <div>{row.name}</div>
              </a>
            )
          })}
        </div>
        }
      </div>
    )
  }
}


function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TextAreaFacebook);
