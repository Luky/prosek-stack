#!/usr/bin/env bash


function e() {
	echo -e $DCKSHL_LOGO"[${CLR_YELLOW}${1}${CLR_END}] ""$2"
}

eVerbose() {
	if [[ $OPT_VERBOSE = "1" ]]; then
		e "$@"
	fi
}

function hr() {
	e $1 "-------------------------------------------"
}

now() {
  date +"%y%m%d-%H%M%S"
}


## Create file if doesn't exists
touchFile() {
	if [ -e $1 ]
	then
		eVerbose File "$1 - Exists"
	else
		eVerbose File "$1 - Created"
		touch $1
	fi
}

# Check if User is not ROOT
rootCheck() {
	if [ $UID -eq 0 ]
	then
		e "System" "Do not run me under root"
		exit 1
	fi
}


copyIdea() {
	eVerbose "System" "Copy .idea files"
	cp -R $DIR/_dev/idea/. $DIR/.idea/
}


dockerShellSelfUpdate() {
	git clean -df
	git remote -v >> $1/docker.sh.log 2>&1
	git fetch --all --tags --prune >> $1/docker.sh.log 2>&1
	git checkout tags/$2 >> $1/docker.sh.log 2>&1
}

dockerShellCompareVersions() {
	if [[ $DCKSHL_VERSION != $DOCKER_TOOLS_VERSION ]]; then
		e System "Docker tools version mistmatch."
		e System "Installed: $DCKSHL_VERSION, Required: $DOCKER_TOOLS_VERSION"
		exit 0
	fi
}
