const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const lessLoader = {
	test: /\.(less)$/,
	use: ExtractTextPlugin.extract({
		fallback: 'style-loader',
		use: ['css-loader', 'postcss-loader', 'less-loader'],
	}),
};

const sassLoader = {
	test: /\.(scss|sass)$/,
	use: ExtractTextPlugin.extract({
		fallback: 'style-loader',
		use: ['css-loader', 'postcss-loader', 'sass-loader'],
	}),
};

const cssLoader = {
	test: /\.(css)$/,
	use: ExtractTextPlugin.extract({
		fallback: 'style-loader',
		use: ['css-loader', 'postcss-loader'],
	}),
};

const fontLoader = {
	test: /\.(eot|svg|ttf|woff|woff2)$/,
	loader: 'file-loader?name=[hash:5].[ext]',
};


const jqueryLoader = {
	test: require.resolve('jquery'),
	use: [
		{
			loader: 'expose-loader',
			options: 'jQuery',
		},
		{
			loader: 'expose-loader',
			options: '$',
		},
	],
};

const jsxLoader = {
	test: /\.(js|jsx)$/,
	exclude: /node_modules/,
	use: [
		{
			loader: 'babel-loader',
		},
	],
};

const imageLoader = {
	test: /\.(jpg|png)$/,
	use: [
		{
			loader: 'url-loader',
			options: {
				limit: 2500,
			},
		},
	],
};


module.exports = {
	entry: {
		bundle: __dirname + '/../resources/index.js',
	},
	output: {
		path: path.resolve(__dirname, './../www/assets'),
		filename: '[name].js',
		chunkFilename: '[name].chunk.js',
	},
	module: {
		rules: [
			fontLoader,
			imageLoader,
			jsxLoader,
			lessLoader,
			sassLoader,
			cssLoader,
			jqueryLoader,
			{
				test: require.resolve('toastr'),
				use: [
					{
						loader: 'expose-loader',
						options: 'toastr',
					},
				],
			},
			{
				test: require.resolve('nette-forms'),
				use: [
					{
						loader: 'expose-loader',
						options: 'Nette',
					},
				],
			},
			{
				test: require.resolve('react'),
				use: [
					{
						loader: 'expose-loader',
						options: 'React',
					},
				],
			},
			{
				test: require.resolve('react-dom'),
				use: [
					{
						loader: 'expose-loader',
						options: 'ReactDOM',
					},
				],
			},
		],
	},
	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			children: true,
			minChunks: 2,
			async: true,
		}),
		new webpack.optimize.OccurrenceOrderPlugin(true),
		new webpack.LoaderOptionsPlugin({
			//minimize: true,
			minimize: false,
			debug: true,
		}),

		// Extract the CSS into a separate file
		new ExtractTextPlugin('styles.css'),

		//new webpack.optimize.UglifyJsPlugin({
		//    compressor: {
		//        //warnings: false,
		//        warnings: true,
		//    },
		//}),
	],
};
