const lessLoader = {
	test: /\.(less)$/,
	use: ExtractTextPlugin.extract({
		fallback: 'style-loader',
		use: ['css-loader', 'postcss-loader', 'less-loader'],
	}),
};

const sassLoader = {
	test: /\.(scss|sass)$/,
	use: ExtractTextPlugin.extract({
		fallback: 'style-loader',
		use: ['css-loader', 'postcss-loader', 'sass-loader'],
	}),
};

const cssLoader = {
	test: /\.(css)$/,
	use: ExtractTextPlugin.extract({
		fallback: 'style-loader',
		use: ['css-loader', 'postcss-loader'],
	}),
};

const fontLoader = {
	test: /\.(eot|svg|ttf|woff|woff2)$/,
	loader: 'file-loader?name=[hash:5].[ext]',
};


const jqueryLoader = {
	test: require.resolve('jquery'),
	use: [
		{
			loader: 'expose-loader',
			options: 'jQuery',
		},
		{
			loader: 'expose-loader',
			options: '$',
		},
	],
};

const jsxLoader = {
	test: /\.(js|jsx)$/,
	exclude: /node_modules/,
	use: [
		{
			loader: 'babel-loader',
		},
	],
};

const imageLoader = {
	test: /\.(jpg|png)$/,
	use: [
		{
			loader: 'url-loader',
			options: {
				limit: 2500,
			},
		},
	],
};
